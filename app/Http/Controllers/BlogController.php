<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function getArchive()
    {
        return '<h1>Halaman Archive</h1>';
    }

    public function postUpdate()
    {
        return 'Menyimpan article..';
    }
}
